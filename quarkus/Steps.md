here are the steps to test the project:
1. run the project excel-uploader-web(use intellij preferably)
2. after project is run open a browser and go to http://localhost:8080/q/dev-ui/extensions
3. search for the openID Connect and click the keycloak provider
4. login using username admin and password admin
5. after finished login click the access token and click the clipboard button
6. open the ifg collection inside of resources directory and use the token that was just copied as a bearer token