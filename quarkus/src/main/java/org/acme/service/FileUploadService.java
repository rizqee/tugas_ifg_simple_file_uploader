package org.acme.service;

import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedMap;
import org.apache.commons.io.IOUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Singleton
public class FileUploadService {
//    @ConfigProperty(name = "upload.directory")
    String UPLOAD_DIR = System.getProperty("user.dir")+"/src/main/resources/data".replace("/",File.separator);

    public String uploadFile(MultipartFormDataInput input){
        Map<String,List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get("file");
        String fileName = "";
        for (InputPart inputPart : inputParts){
            try {
                MultivaluedMap<String,String> header = inputPart.getHeaders();
                fileName = getFileName(header);
                InputStream inputStream = inputPart.getBody(InputStream.class,null);
                writeFile(inputStream,fileName);
            }catch (IOException  e ){
                if (e instanceof FileAlreadyExistsException){
                    return "File already existed on directory";
                }
                e.printStackTrace();
            }
        }
        return "Files Successfully Uploaded";
    }
    public String getFileName(MultivaluedMap<String,String> header){
        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
        for(String fileName:contentDisposition){
            if(fileName.trim().startsWith("filename")){
                String[] name = fileName.split("=");
                return name[1].trim().replaceAll("\"","");
            }
        }
        return "";
    }
    public void writeFile(InputStream inputStream,String fileName) throws IOException{
        byte[] bytes = IOUtils.toByteArray(inputStream);
        File customDir = new File(UPLOAD_DIR);
        fileName = customDir.getAbsolutePath()+File.separator+fileName;
        Files.write(Paths.get(fileName),bytes, StandardOpenOption.CREATE_NEW);
    }
}
