package org.acme.resource;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import org.jboss.resteasy.annotations.cache.NoCache;

@Path("/api/users")
public class UserResource {
    @Inject
    SecurityIdentity securityIdentity;

    @GET
    @Path("/me")
    @RolesAllowed("user")
    @NoCache
    public User me(){
        return new User(securityIdentity);
    }
    public static class User{
        private final String userName;
        User(SecurityIdentity securityIdentity){
            this.userName = securityIdentity.getPrincipal().getName();
        }
        public String getUserName(){
            return this.userName;
        }
    }
}
